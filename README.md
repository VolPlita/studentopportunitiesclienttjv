## Running application:

   Semester project for BIE-TJV

 - Build the application using gradle with command (./gradlew build). It make take time as it is running all the tests.
 - Run the application with command (./gradlew bootRun)

 ### Running at:
 - http://localhost:9090/docs-api

 #### This is client application, there is server here: https://gitlab.com/VolPlita/studentopportunitiesapitjv