package cz.cvut.fit.tjv.studentopportunitiesclient.model;

import java.time.Duration;
import java.time.LocalDateTime;

import lombok.*;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
@ToString
public class OpportunityDto {
    private Long id;
    private String title;
    private String description;
    private LocalDateTime start;
    private LocalDateTime finish;
}
