package cz.cvut.fit.tjv.studentopportunitiesclient.services;

import cz.cvut.fit.tjv.studentopportunitiesclient.api_client.ReviewClient;
import cz.cvut.fit.tjv.studentopportunitiesclient.model.ReviewDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ReviewService {
    ReviewClient client;

    @Autowired
    public ReviewService(ReviewClient client) {
        this.client = client;
    }

    public ReviewDto create(ReviewDto reviewDto, String username, Long id) {
        return client.create(reviewDto, username, id);
    }

    public Iterable<ReviewDto> readAll() {
        return client.readAll();
    }

    public ReviewDto readById(Long id) {
        return client.readById(id);
    }

    public void editReview(Long id, String username, ReviewDto reviewDto) {
        client.editReview(id, username, reviewDto);
    }

    public void deleteReview(Long id, String username) {
        client.deleteReview(id, username);
    }

    public Iterable<ReviewDto> readAllReviewsOfOpportunity(Long id) {
        return client.readAllReviewsOfOpportunity(id);
    }
}
