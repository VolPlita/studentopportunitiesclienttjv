package cz.cvut.fit.tjv.studentopportunitiesclient.controllers;

import cz.cvut.fit.tjv.studentopportunitiesclient.model.UserDto;
import cz.cvut.fit.tjv.studentopportunitiesclient.services.UserService;
import jakarta.servlet.http.HttpSession;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.HttpClientErrorException;

import java.util.stream.Stream;
import java.util.stream.StreamSupport;

@Controller
@RequestMapping("/user")
public class UserController {
    private final UserService service;

    private String loggedInUsername;

    public UserController(UserService service) {
        this.service = service;
    }

    // Sign up
    @GetMapping("/signup")
    public String signUpForm(Model model) {
        model.addAttribute("userDto", new UserDto());
        return "user/signup";
    }

    @PostMapping("/signup")
    public String signUpUser(@ModelAttribute("userDto") UserDto userDto, Model model) {
        try {
            service.create(userDto);
        } catch (HttpClientErrorException.Conflict e) {
            return showSignupFail(model, userDto);
        }
        return showSignupSuccess(model, userDto);
    }

    private String showSignupSuccess(Model model, UserDto userDto) {
        model.addAttribute("userDto",userDto);
        return "user/signup-success";
    }

    private String showSignupFail(Model model, UserDto userDto) {
        model.addAttribute("userDto",userDto);
        return "user/signup-fail";
    }

    // Log in
    @GetMapping("/login")
    public String logInForm(Model model) {
        model.addAttribute("username", "");
        return "user/login";
    }

    @PostMapping("/login")
    public String logInUser(@ModelAttribute("username") String username, Model model, HttpSession session) {
        Stream<UserDto> allUsers = StreamSupport.stream(service.readAll().spliterator(), false);
        if (allUsers.anyMatch(user -> user.getUsername().equals(username))) {
            loggedInUsername = username;
            session.setAttribute("loggedInUsername", loggedInUsername);
            model.addAttribute("loggedInUsername", loggedInUsername);
            return "index";
        }
        else {
            return showLoginFail(model, username);
        }
    }

    private String showLoginFail(Model model, String username) {
        model.addAttribute("username", username);
        return "user/login-fail";
    }
}
