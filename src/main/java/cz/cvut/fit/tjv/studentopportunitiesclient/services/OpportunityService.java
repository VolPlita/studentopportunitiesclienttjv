package cz.cvut.fit.tjv.studentopportunitiesclient.services;

import cz.cvut.fit.tjv.studentopportunitiesclient.api_client.OpportunityClient;
import cz.cvut.fit.tjv.studentopportunitiesclient.model.OpportunityDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class OpportunityService {
    OpportunityClient client;

    @Autowired
    public OpportunityService(OpportunityClient client) {
        this.client = client;
    }

    public OpportunityDto create(OpportunityDto opportunityDto, String username) {
        return client.create(opportunityDto, username);
    }

    public Iterable<OpportunityDto> findAllOrEnrolledByUsernameOrRatedByUsername(String username, Integer rating) {
        return client.findAllOrEnrolledByUsernameOrRatedByUsername(username, rating);
    }

    public OpportunityDto readById(Long id) {
        return client.readById(id);
    }

    public void update (Long id, OpportunityDto opportunityDto, String username) {
        client.update(id, opportunityDto, username);
    }

    public void delete (Long id, String username) {
        client.delete(id, username);
    }

    public void enrollStudentInOpportunity(Long id, String username) {
        client.enrollStudentInOpportunity(id, username);
    }

    public void removeStudentFromOpportunity(Long id, String username) {
        client.removeStudentFromOpportunity(id, username);
    }
}
