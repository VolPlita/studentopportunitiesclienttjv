package cz.cvut.fit.tjv.studentopportunitiesclient.api_client;

import cz.cvut.fit.tjv.studentopportunitiesclient.model.OpportunityDto;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestClient;

import java.util.Arrays;
import java.util.Objects;

@Component
public class OpportunityClient {
    private final RestClient restClient;
    private final String baseUrl;

    public OpportunityClient(@Value("http://localhost:8080") String baseUrl) {
        this.baseUrl = baseUrl + "/opportunity";
        this.restClient = RestClient.create(this.baseUrl);
    }

    public OpportunityDto create(OpportunityDto opportunityDto, String username) {
        return restClient
                .post()
                .uri(baseUrl + "?username=" + username)
                .contentType(MediaType.APPLICATION_JSON)
                .body(opportunityDto)
                .retrieve()
                .toEntity(OpportunityDto.class)
                .getBody();
    }

    public Iterable<OpportunityDto> findAllOrEnrolledByUsernameOrRatedByUsername(String username, Integer rating) {
        if (username == null || username.isEmpty() && rating == null) {
            return Arrays.asList(Objects.requireNonNull(
                    restClient
                            .get()
                            .uri(baseUrl)
                            .retrieve()
                            .toEntity(OpportunityDto[].class)
                            .getBody()));
        }
        if (rating == null) {
            return Arrays.asList(Objects.requireNonNull(
                    restClient
                            .get()
                            .uri(baseUrl + "?username=" + username)
                            .retrieve()
                            .toEntity(OpportunityDto[].class)
                            .getBody()));
        }
        return Arrays.asList(Objects.requireNonNull(
                restClient
                        .get()
                        .uri(baseUrl + "?username=" + username + "&rating=" + rating)
                        .retrieve()
                        .toEntity(OpportunityDto[].class)
                        .getBody()));
    }

    public OpportunityDto readById(Long id) {
        return restClient
                .get()
                .uri(baseUrl + "/" + id)
                .retrieve()
                .toEntity(OpportunityDto.class)
                .getBody();
    }

    public void update (Long id, OpportunityDto opportunityDto, String username) {
        restClient
                .put()
                .uri(baseUrl + "/" + id + "?username=" + username)
                .contentType(MediaType.APPLICATION_JSON)
                .body(opportunityDto)
                .retrieve()
                .toBodilessEntity();
    }

    public void delete (Long id, String username) {
        restClient
                .delete()
                .uri(baseUrl + "/" + id + "?username=" + username)
                .retrieve()
                .toBodilessEntity();
    }

    public void enrollStudentInOpportunity(Long id, String username) {
        restClient
                .put()
                .uri(baseUrl + "/" + id + "/enroll" + "?username=" + username)
                .retrieve()
                .toBodilessEntity();
    }

    public void removeStudentFromOpportunity(Long id, String username) {
        restClient
                .put()
                .uri(baseUrl + "/" + id + "/remove" + "?username=" + username)
                .retrieve()
                .toBodilessEntity();
    }
}
