package cz.cvut.fit.tjv.studentopportunitiesclient.api_client;

import cz.cvut.fit.tjv.studentopportunitiesclient.model.UserDto;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestClient;

import java.util.Arrays;
import java.util.Objects;

@Component
public class UserClient {
    private final RestClient restClient;
    private final String baseUrl;

    public UserClient(@Value("http://localhost:8080") String baseUrl) {
        this.baseUrl = baseUrl + "/user";
        this.restClient = RestClient.create(this.baseUrl);
    }

        public UserDto create(UserDto userDto) {
            return restClient
                    .post()
                    .contentType(MediaType.APPLICATION_JSON)
                    .body(userDto)
                    .retrieve()
                    .toEntity(UserDto.class)
                    .getBody();
        }

        public Iterable<UserDto> readAll() {
            return Arrays.asList(Objects.requireNonNull(
                    restClient
                            .get()
                            .uri(baseUrl)
                            .retrieve()
                            .toEntity(UserDto[].class)
                            .getBody()));
        }

        public UserDto readById(String username) {
            return restClient
                    .get()
                    .uri(baseUrl + "/" + username)
                    .retrieve()
                    .toEntity(UserDto.class)
                    .getBody();
        }

        public void update(String username, UserDto userDto) {
            restClient
                    .put()
                    .uri(baseUrl + "/" + username)
                    .contentType(MediaType.APPLICATION_JSON)
                    .body(userDto)
                    .retrieve()
                    .toBodilessEntity();
        }

        public void deleteById(String username) {
            restClient
                    .delete()
                    .uri(baseUrl + "/" + username)
                    .retrieve()
                    .toBodilessEntity();
        }
}
