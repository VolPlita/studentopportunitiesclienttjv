package cz.cvut.fit.tjv.studentopportunitiesclient.controllers;

import cz.cvut.fit.tjv.studentopportunitiesclient.model.ReviewDto;
import cz.cvut.fit.tjv.studentopportunitiesclient.services.ReviewService;
import jakarta.servlet.http.HttpSession;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.HttpClientErrorException;

@Controller
@RequestMapping("/review")
public class ReviewController {
    private final ReviewService service;

    public ReviewController(ReviewService service) {
        this.service = service;
    }

    // Make a review

    @PostMapping("/makereview")
    public String makeReview(@ModelAttribute("reviewDto") ReviewDto reviewDto,
                             @RequestParam("opportunityId") Long opportunityId,
                             HttpSession session) {
        String loggedInUsername = (String) session.getAttribute("loggedInUsername");
        try {
            service.create(reviewDto,
                    loggedInUsername,
                    opportunityId);
        } catch (HttpClientErrorException.Forbidden e) {
            return "review/fail";
        }
        return "review/success";
    }
}
