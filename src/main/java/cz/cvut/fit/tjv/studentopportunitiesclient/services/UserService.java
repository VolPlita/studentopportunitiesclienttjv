package cz.cvut.fit.tjv.studentopportunitiesclient.services;

import cz.cvut.fit.tjv.studentopportunitiesclient.api_client.UserClient;
import cz.cvut.fit.tjv.studentopportunitiesclient.model.UserDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class UserService {
    UserClient client;

    @Autowired
    public UserService(UserClient client) {
        this.client = client;
    }

    public UserDto create(UserDto userDto) {
        return client.create(userDto);
    }

    public Iterable<UserDto> readAll() {
        return client.readAll();
    }

    public UserDto readById(String username) {
        return client.readById(username);
    }

    public void update(String username, UserDto userDto) {
        client.update(username, userDto);
    }

    public void deleteById(String username) {
        client.deleteById(username);
    }
}
