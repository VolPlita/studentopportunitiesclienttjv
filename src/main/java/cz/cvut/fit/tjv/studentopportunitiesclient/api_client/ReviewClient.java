package cz.cvut.fit.tjv.studentopportunitiesclient.api_client;

import cz.cvut.fit.tjv.studentopportunitiesclient.model.ReviewDto;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestClient;

import java.util.Arrays;
import java.util.Objects;

@Component
public class ReviewClient {
    private final RestClient restClient;
    private final String baseUrl;

    public ReviewClient(@Value("http://localhost:8080") String baseUrl) {
        this.baseUrl = baseUrl + "/review";
        this.restClient = RestClient.create(this.baseUrl);
    }

    public ReviewDto create(ReviewDto reviewDto, String username, Long id) {
        return restClient
                .post()
                .uri(baseUrl + "?username=" + username + "&id=" + id)
                .contentType(MediaType.APPLICATION_JSON)
                .body(reviewDto)
                .retrieve()
                .toEntity(ReviewDto.class)
                .getBody();
    }

    public Iterable<ReviewDto> readAll() {
        return Arrays.asList(Objects.requireNonNull(restClient
                .get()
                .uri(baseUrl)
                .retrieve()
                .toEntity(ReviewDto[].class)
                .getBody()));
    }

    public ReviewDto readById(Long id) {
        return restClient
                .get()
                .uri(baseUrl + "/" + id)
                .retrieve()
                .toEntity(ReviewDto.class)
                .getBody();
    }

    public void editReview(Long id, String username, ReviewDto reviewDto) {
        restClient
                .put()
                .uri(baseUrl + "/" + id + "?username=" + username)
                .body(reviewDto)
                .contentType(MediaType.APPLICATION_JSON)
                .retrieve()
                .toBodilessEntity();
    }

    public void deleteReview(Long id, String username) {
        restClient
                .delete()
                .uri(baseUrl + "/" + id + "?username=" + username)
                .retrieve()
                .toBodilessEntity();
    }

    public Iterable<ReviewDto> readAllReviewsOfOpportunity(Long id) {
        return Arrays.asList(Objects.requireNonNull(restClient
                .get()
                .uri(baseUrl + "/of?opportunityId=" + id)
                .retrieve()
                .toEntity(ReviewDto[].class)
                .getBody()));
    }
}
