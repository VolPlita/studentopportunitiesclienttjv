package cz.cvut.fit.tjv.studentopportunitiesclient.controllers;

import cz.cvut.fit.tjv.studentopportunitiesclient.model.OpportunityDto;
import cz.cvut.fit.tjv.studentopportunitiesclient.model.ReviewDto;
import cz.cvut.fit.tjv.studentopportunitiesclient.services.OpportunityService;
import cz.cvut.fit.tjv.studentopportunitiesclient.services.ReviewService;
import jakarta.servlet.http.HttpSession;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestClient;

import java.util.stream.StreamSupport;

@Controller
@RequestMapping("/opportunity")
public class OpportunityController {
    private final OpportunityService service;
    private final ReviewService reviewService;

    public OpportunityController(OpportunityService service, ReviewService reviewService) {
        this.service = service;
        this.reviewService = reviewService;
    }

    // Create
    @GetMapping("/create")
    public String createOpportunityForm(Model model, HttpSession session) {
        model.addAttribute("opportunityDto", new OpportunityDto());
        model.addAttribute("session", session);
        return "opportunity/create";
    }

    @PostMapping("/create")
    public String createOpportunity(@ModelAttribute("opportunityDto") OpportunityDto opportunityDto,
                                    Model model,
                                    HttpSession session) {
        try {
            service.create(opportunityDto, (String) session.getAttribute("loggedInUsername"));
        } catch (HttpClientErrorException.Forbidden | HttpClientErrorException.Conflict e) {
            return showCreateFail(model, (String) session.getAttribute("loggedInUsername"));
        }
        return "index";
    }

    private String showCreateFail(Model model, String loggedInUsername) {
        model.addAttribute("loggedInUsername", loggedInUsername);
        return "opportunity/create-fail";
    }

    // Enroll
//    @PathVariable Long id, @RequestParam("username") String username
    @PostMapping("/{id}/enroll")
    public String enrollInOpportunity(@PathVariable Long id, HttpSession session, Model model) {
        String loggedInUsername = (String) session.getAttribute("loggedInUsername");
        try {
            service.enrollStudentInOpportunity(id, loggedInUsername);
        } catch (HttpClientErrorException e) {
            return "error";
        }
        return showEnrollSuccess(model, loggedInUsername, id);
    }

    private String showEnrollSuccess(Model model, String username, Long opportunityId) {
        model.addAttribute("username", username);
        model.addAttribute("title", service.readById(opportunityId).getTitle());
        return "opportunity/enroll-success";
    }

    // Remove

    // Show

    @GetMapping("/show")
    public String showAllOpportunities(Model model) {
        Iterable<OpportunityDto> allOpportunities = service.findAllOrEnrolledByUsernameOrRatedByUsername(null, null);
//        if (!allOpportunities.iterator().hasNext()) throw new RuntimeException("HUYNIaaaaa");
        model.addAttribute("allOpportunities", allOpportunities);
        return "opportunity/opportunities";
    }

    @GetMapping("/enrolled")
    public String showEnrolledForUsername(HttpSession session, Model model) {
        String username = (String) session.getAttribute("loggedInUsername");
        Iterable<OpportunityDto> enrolledOpportunities =
                service.findAllOrEnrolledByUsernameOrRatedByUsername(
                        username,
                        null);
        model.addAttribute("enrolledOpportunities", enrolledOpportunities);
        model.addAttribute("username", username);
        return "opportunity/enrolled-opportunities";
    }

    // Make review

    @GetMapping("/makereview/{opportunityId}")
    public String makeReviewForm(@PathVariable("opportunityId") Long opportunityId,
                                 Model model) {
        model.addAttribute("reviewDto", new ReviewDto());
        model.addAttribute("opportunityId", opportunityId);
        return "review/create";
    }

    @GetMapping("/{opportunityId}/allreviews")
    public String showReviews(@PathVariable("opportunityId") Long opportunityId,
                              Model model) {
        Iterable<ReviewDto> allReviews = reviewService.readAllReviewsOfOpportunity(opportunityId);
        model.addAttribute("reviews", allReviews);
        model.addAttribute("opportunityId", opportunityId);
        return "review/reviews";
    }

    // Enroll other user's enrolled opportunities

    @GetMapping("/friend")
    public String logInForm(Model model) {
        model.addAttribute("username", "");
        return "opportunity/friend";
    }

    @PostMapping("/friend")
    public String enrollOtherStudentsEnrolledOpportunities(@ModelAttribute("username") String username,
                                                           HttpSession session) {
        try {
            String currentUsername = (String) session.getAttribute("loggedInUsername");
            Iterable<OpportunityDto> enrolledByUsername =
                    service.findAllOrEnrolledByUsernameOrRatedByUsername(username, null);
            Iterable<OpportunityDto> enrolledByCurrentUsername =
                    service.findAllOrEnrolledByUsernameOrRatedByUsername(currentUsername, null);
            for (OpportunityDto opportunityDto : enrolledByUsername) {
                if (StreamSupport.stream(enrolledByCurrentUsername.spliterator(), false)
                        .noneMatch(element -> element.equals(opportunityDto))) {
                    service.enrollStudentInOpportunity(opportunityDto.getId(), currentUsername);
                }
            }
        } catch (HttpClientErrorException e) {
            return "error";
        }
        return "index";
    }
}
