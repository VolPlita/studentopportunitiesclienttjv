package cz.cvut.fit.tjv.studentopportunitiesclient.model;

import lombok.*;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
@ToString
public class ReviewDto {
    private Long id;
    private int rating;
    private String comment;
}
